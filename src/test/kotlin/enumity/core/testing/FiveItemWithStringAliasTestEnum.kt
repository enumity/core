package enumity.core.testing

import enumity.core.alias.StringAliased

enum class FiveItemWithStringAliasTestEnum(private val alias: String) : StringAliased {
    First("ghf"),
    Second("yae"),
    Third("iapsl"),
    Fourth("tayrwus"),
    Fifth("paosgur")
    ;

    override fun alias(): String? = alias
}