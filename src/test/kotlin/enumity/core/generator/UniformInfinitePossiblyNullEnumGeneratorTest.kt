package enumity.core.generator

import enumity.core.testing.EnumGeneratorTesting
import enumity.core.testing.EnumGeneratorTesting.allOfAndNull
import enumity.core.testing.FiveItemTestEnum
import org.junit.jupiter.api.Test

internal class UniformInfinitePossiblyNullEnumGeneratorTest {

    @Test
    fun testSpan() {
        val testEnum = FiveItemTestEnum::class.java
        val generator = UniformInfinitePossiblyNullEnumGenerator(testEnum)
        EnumGeneratorTesting.testSpan(
            enumClass = testEnum,
            expectedSpan = allOfAndNull(testEnum)
        ) {
            generator.nextPossiblyNull()
        }
    }
}