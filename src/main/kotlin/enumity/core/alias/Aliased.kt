package enumity.core.alias

interface Aliased<A> {
    fun alias(): A
}