package enumity.core.generator

import enumity.core.testing.EnumGeneratorTesting
import enumity.core.testing.FiveItemTestEnum
import org.junit.jupiter.api.Test
import java.util.*

internal class UniformInfiniteNonNullEnumGeneratorTest {

    @Test
    fun testSpan() {
        val testEnum = FiveItemTestEnum::class.java
        val generator = UniformInfiniteNonNullEnumGenerator(testEnum)
        EnumGeneratorTesting.testSpan(
            enumClass = testEnum,
            expectedSpan = EnumSet.allOf(testEnum)
        ) {
            generator.next()
        }
    }
}