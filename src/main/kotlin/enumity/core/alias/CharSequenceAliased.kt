package enumity.core.alias

interface CharSequenceAliased<A> : Aliased<A>
        where A : CharSequence?