package enumity.core.alias

interface EnumLookupByCharSequenceAlias<E, A> : LookupByCharSequenceAlias<E, A>
        where E : Enum<E>,
              E : CharSequenceAliased<A>,
              A : CharSequence?