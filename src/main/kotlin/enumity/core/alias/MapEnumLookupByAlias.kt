package enumity.core.alias

class MapEnumLookupByAlias<E, A>(enumClass: Class<E>) : LookupByAlias<E, A>
        where E : Enum<E>,
              E : Aliased<A> {

    private val lookupMap: Map<A, E>

    init {
        lookupMap = enumClass.enumConstants.associateBy { it.alias() }
    }

    override fun lookupByAlias(alias: A): E? = lookupMap[alias]
}