package enumity.core.alias

object AliasedContracts {

    fun <E, A> obeysUniquenessContract(enum: Class<E>): Boolean
            where E : Enum<E>,
                  E : Aliased<A> =
        enum.enumConstants.map { it.alias() }.distinct().count() == enum.enumConstants.size

    fun <E, A> obeysNonNullContract(enum: Class<E>): Boolean
            where E : Enum<E>,
                  E : Aliased<A> =
        enum.enumConstants.none { it.alias() == null }

    fun <E, A> obeysNonNullAndNonBlankContract(enum: Class<E>): Boolean
            where E : Enum<E>,
                  E : CharSequenceAliased<A>,
                  A : CharSequence? =
        enum.enumConstants.none { it.alias().isNullOrBlank() }
}