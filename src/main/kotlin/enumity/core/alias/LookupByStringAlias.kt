package enumity.core.alias

interface LookupByStringAlias<T> : LookupByCharSequenceAlias<T, String?>
        where T : CharSequenceAliased<String?>