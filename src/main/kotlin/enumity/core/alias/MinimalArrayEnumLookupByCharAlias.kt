package enumity.core.alias

import enumity.core.extension.aliasRange

class MinimalArrayEnumLookupByCharAlias<E>(enumClass: Class<E>) : EnumLookupByCharAlias<E>
        where E : Enum<E>,
              E : CharAliased {

    private val indexOffset: Char
    private val lookupArray: Array<Any?>

    init {
        val aliasRange = enumClass.aliasRange()
        indexOffset = aliasRange.first
        lookupArray = Array(aliasRange.count()) { null }
        enumClass.enumConstants.forEach { lookupArray[it.alias().code - indexOffset.code] = it }
    }

    // See https://stackoverflow.com/questions/41941102/instantiating-generic-array-in-kotlin/41946516#41946516
    @Suppress("UNCHECKED_CAST")
    override fun lookupByAlias(alias: Char): E? {
        val shiftedAlias = alias - indexOffset
        if (shiftedAlias < 0 || shiftedAlias > lookupArray.size - 1) return null
        return lookupArray[shiftedAlias] as E?
    }
}