package enumity.core.alias

interface EnumLookupByStringAlias<E> : EnumLookupByCharSequenceAlias<E, String?>
        where E : Enum<E>,
              E : StringAliased