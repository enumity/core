package enumity.core.generator

interface InfinitePossiblyNullEnumGenerator<E>
        where E : Enum<E> {
    fun nextPossiblyNull(): E?
}