package enumity.core.testing

enum class FiveItemTestEnum {
    First,
    Second,
    Third,
    Fourth,
    Fifth
}