package enumity.core.generator

import enumity.core.testing.EnumGeneratorTesting
import enumity.core.testing.FiveItemTestEnum
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.test.assertNotNull
import kotlin.test.assertNull

internal class UniformNonRepeatingNonNullEnumGeneratorTest {

    @Test
    fun testSpan() {
        val testEnum = FiveItemTestEnum::class.java
        val generator = UniformNonRepeatingNonNullEnumGenerator(testEnum)
        EnumGeneratorTesting.testSpan(
            enumClass = testEnum,
            expectedSpan = EnumSet.allOf(testEnum),
            maxIterations = testEnum.enumConstants.size
        ) {
            generator.nextPossiblyNullIfExhausted()
        }
    }

    @Test
    fun testNullAfterFullSpan() {
        val testEnum = FiveItemTestEnum::class.java
        val generator = UniformNonRepeatingNonNullEnumGenerator(testEnum)
        for (i in 0 until testEnum.enumConstants.size) {
            assertNotNull(generator.nextPossiblyNullIfExhausted())
        }
        for (i in 0 until 5) {
            assertNull(generator.nextPossiblyNullIfExhausted())
        }
    }
}