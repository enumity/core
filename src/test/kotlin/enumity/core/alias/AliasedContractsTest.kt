package enumity.core.alias

import org.junit.jupiter.api.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class AliasedContractsTest {

    @Test
    fun `obeys uniqueness contract fails when a duplicate alias is encountered`() {
        assertFalse(AliasedContracts.obeysUniquenessContract(WithDuplicateButOnlyNonNullAliases::class.java))
    }

    @Test
    fun `obeys uniqueness contract succeeds when all aliases are distinct`() {
        assertTrue(AliasedContracts.obeysUniquenessContract(WithOnlyDistinctAliases::class.java))
    }

    @Test
    fun `obeys non-null contract succeeds when all aliases are non-null`() {
        assertTrue(AliasedContracts.obeysNonNullContract(WithDuplicateButOnlyNonNullAliases::class.java))
    }

    @Test
    fun `obeys non-null contract fails when an alias is null`() {
        assertFalse(AliasedContracts.obeysNonNullContract(WithNullAlias::class.java))
    }

    private enum class WithDuplicateButOnlyNonNullAliases(private val alias: String?) : StringAliased {
        First("A"),
        Second("B"),
        Third("A")
        ;

        override fun alias(): String? = alias
    }

    private enum class WithOnlyDistinctAliases(private val alias: String?) : StringAliased {
        First("A"),
        Second("B"),
        Third("C")
        ;

        override fun alias(): String? = alias
    }

    private enum class WithNullAlias(private val alias: String?): StringAliased {
        First("A"),
        Second(null),
        Third("C")
        ;

        override fun alias(): String? = alias
    }
}