package enumity.core.alias

interface EnumLookupByCharAlias<E> : LookupByAlias<E, Char>
        where E : Enum<E>,
              E : CharAliased