package enumity.core.alias

interface LookupByAlias<T, A>
        where T : Aliased<A> {
    fun lookupByAlias(alias: A): T?
}