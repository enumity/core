package enumity.core.generator

interface InfiniteNonNullEnumGenerator<E>
        where E : Enum<E> {
    fun next(): E
}