package enumity.core.generator

import kotlin.random.Random
import kotlin.random.nextInt

class UniformInfinitePossiblyNullEnumGenerator<E>(enumClass: Class<E>) : InfinitePossiblyNullEnumGenerator<E>
        where E : Enum<E> {

    private val enumValues = enumClass.enumConstants
    private val ordinalRange: IntRange = IntRange(0, enumValues.size)

    override fun nextPossiblyNull(): E? {
        val ordinal = Random.nextInt(ordinalRange)
        return if (ordinal == ordinalRange.last) null else enumValues[ordinal]
    }
}