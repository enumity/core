package enumity.core.extension

import enumity.core.alias.CharAliased

fun <E> Class<E>.aliasRange(): CharRange
        where E : Enum<E>,
              E : CharAliased {
    var min = Char.MAX_VALUE
    var max = Char.MIN_VALUE
    for (enumConstant in this.enumConstants) {
        val alias = enumConstant.alias()
        if (alias < min) min = alias
        if (alias > max) max = alias
    }
    return CharRange(min, max)
}