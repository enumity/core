package enumity.core.alias

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class MinimalArrayEnumLookupByCharAliasTest {

    @Test
    fun testBeforeMinChar() {
        val lookup = MinimalArrayEnumLookupByCharAlias(TestEnum1::class.java)
        assertNull(lookup.lookupByAlias('F'))
    }

    @Test
    fun testAfterMaxChar() {
        val lookup = MinimalArrayEnumLookupByCharAlias(TestEnum1::class.java)
        assertNull(lookup.lookupByAlias('Y'))
    }

    @Test
    fun testNonAliasBetweenMinAndMaxChar() {
        val lookup = MinimalArrayEnumLookupByCharAlias(TestEnum1::class.java)
        assertNull(lookup.lookupByAlias('H'))
        assertNull(lookup.lookupByAlias('K'))
    }

    @Test
    fun testSuccessfulLookups() {
        val lookup = MinimalArrayEnumLookupByCharAlias(TestEnum1::class.java)
        assertEquals(TestEnum1.First, lookup.lookupByAlias('J'))
        assertEquals(TestEnum1.Second, lookup.lookupByAlias('G'))
        assertEquals(TestEnum1.Third, lookup.lookupByAlias('X'))
    }

    @Test
    fun testAbsoluteMin() {
        val lookup = MinimalArrayEnumLookupByCharAlias(TestEnum1::class.java)
        assertNull(lookup.lookupByAlias(Char.MIN_VALUE))
    }

    @Test
    fun testAbsoluteMax() {
        val lookup = MinimalArrayEnumLookupByCharAlias(TestEnum1::class.java)
        assertNull(lookup.lookupByAlias(Char.MAX_VALUE))
    }

    private enum class TestEnum1(private val alias: Char) : CharAliased {
        First('J'),
        Second('G'),
        Third('X')
        ;

        override fun alias(): Char = alias
    }
}