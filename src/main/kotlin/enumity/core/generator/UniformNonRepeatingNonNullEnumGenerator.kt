package enumity.core.generator

class UniformNonRepeatingNonNullEnumGenerator<E>(enumClass: Class<E>) : NonRepeatingNonNullEnumGenerator<E>
        where E : Enum<E> {

    private val values = enumClass.enumConstants.asList().shuffled()
    private val valuesIterator = values.iterator()

    override fun nextPossiblyNullIfExhausted(): E? = if (valuesIterator.hasNext()) valuesIterator.next() else null
}