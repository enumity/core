package enumity.core.alias

interface LookupByCharSequenceAlias<T, A> : LookupByAlias<T, A>
        where T : CharSequenceAliased<A>,
              A : CharSequence?