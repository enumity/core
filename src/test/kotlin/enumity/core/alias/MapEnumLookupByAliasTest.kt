package enumity.core.alias

import enumity.core.testing.FiveItemWithStringAliasTestEnum
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class MapEnumLookupByAliasTest {

    @Test
    fun testSuccessfulLookups() {
        val lookup = MapEnumLookupByAlias(FiveItemWithStringAliasTestEnum::class.java)
        assertEquals(FiveItemWithStringAliasTestEnum.First, lookup.lookupByAlias("ghf"))
        assertEquals(FiveItemWithStringAliasTestEnum.Second, lookup.lookupByAlias("yae"))
        assertEquals(FiveItemWithStringAliasTestEnum.Third, lookup.lookupByAlias("iapsl"))
        assertEquals(FiveItemWithStringAliasTestEnum.Fourth, lookup.lookupByAlias("tayrwus"))
        assertEquals(FiveItemWithStringAliasTestEnum.Fifth, lookup.lookupByAlias("paosgur"))
    }

    @Test
    fun testFailingLookups() {
        val lookup = MapEnumLookupByAlias(FiveItemWithStringAliasTestEnum::class.java)
        assertNull(lookup.lookupByAlias(null))
        assertNull(lookup.lookupByAlias("  \t "))
        assertNull(lookup.lookupByAlias("ABC"))
    }
}