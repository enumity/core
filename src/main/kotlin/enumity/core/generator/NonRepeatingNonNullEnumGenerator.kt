package enumity.core.generator

interface NonRepeatingNonNullEnumGenerator<E>
        where E : Enum<E> {
    fun nextPossiblyNullIfExhausted(): E?
}