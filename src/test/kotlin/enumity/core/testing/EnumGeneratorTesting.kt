package enumity.core.testing

import org.junit.platform.commons.logging.LoggerFactory
import java.util.EnumSet
import kotlin.test.fail

object EnumGeneratorTesting {

    private val logger = LoggerFactory.getLogger(javaClass)
    private val defaultIterationsPerValue = 10

    fun <E> testSpan(enumClass: Class<E>, expectedSpan: Set<E?>, generator: () -> E?) where E : Enum<E> {
        testSpan(
            enumClass,
            expectedSpan,
            defaultIterationsPerValue * expectedSpan.size,
            generator
        )
    }

    fun <E> testSpan(enumClass: Class<E>, expectedSpan: Set<E?>, maxIterations: Int, generator: () -> E?)
            where E : Enum<E> {
        val currentSpan = HashSet<E?>()
        var i = 0
        while (expectedSpan != currentSpan) {
            if (i >= maxIterations)
                fail("Expected span of $expectedSpan not achieved after $i iterations. Achieved span of $currentSpan")
            currentSpan.add(generator.invoke())
            ++i
        }
        logger.info { "Achieved expected span of $expectedSpan after $i iterations" }
    }

    fun <E> allOfAndNull(enumClass: Class<E>): Set<E?> where E : Enum<E> = HashSet(EnumSet.allOf(enumClass)) + null
}