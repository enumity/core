package enumity.core.alias

interface LookupByCharAlias<T> : LookupByAlias<T, Char>
        where T : CharAliased