package enumity.core.generator

import kotlin.random.Random
import kotlin.random.nextInt

class UniformInfiniteNonNullEnumGenerator<E>(enumClass: Class<E>) : InfiniteNonNullEnumGenerator<E>
        where E : Enum<E> {

    private val enumValues = enumClass.enumConstants
    private val ordinalRange: IntRange = IntRange(0, enumValues.size - 1)

    override fun next(): E = enumValues[Random.nextInt(ordinalRange)]
}